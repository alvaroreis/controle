package br.ufpa.labes.eas.service;

public class NegocioException extends Exception {

    private static final long serialVersionUID = -7173196053734876218L;

    public NegocioException(final String message) {

        super(message);
    }

}
