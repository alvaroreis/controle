package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.TipoCongoDAO;
import br.ufpa.labes.eas.model.TipoCongo;
import br.ufpa.labes.eas.util.Transacional;

public class TipoCongoService implements Serializable {

    private static final long serialVersionUID = 5163868956855067946L;

    @Inject
    private TipoCongoDAO tipoCongoDAO;

    @Transacional
    public void salvar(final TipoCongo tipoCongo) {

        this.tipoCongoDAO.salvar(tipoCongo);
    }

    @Transacional
    public void excluir(final TipoCongo tipoCongo) {

        this.tipoCongoDAO.excluir(tipoCongo);
    }

    public List<TipoCongo> buscarTodos() {

        return this.tipoCongoDAO.buscarTodos();
    }

    public TipoCongo buscarPeloCodigo(final String codigo) {

        return this.tipoCongoDAO.buscarPeloCodigo(codigo);

    }

}
