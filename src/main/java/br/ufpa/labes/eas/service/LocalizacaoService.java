package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.LocalizacaoDAO;
import br.ufpa.labes.eas.model.Localizacao;
import br.ufpa.labes.eas.util.Transacional;

public class LocalizacaoService implements Serializable {

    private static final long serialVersionUID = 3573597285645098844L;

    @Inject
    private LocalizacaoDAO localizacaoDAO;

    @Transacional
    public void salvar(final Localizacao localizacao) {

        this.localizacaoDAO.salvar(localizacao);

    }

    @Transacional
    public void excluir(final Localizacao localizacao) {

        this.localizacaoDAO.excluir(localizacao);

    }

    public List<Localizacao> buscarTodosFilhos() {

        return this.localizacaoDAO.buscarTodosFilhos();
    }

    public List<Localizacao> buscarTodosPai() {

        return this.localizacaoDAO.buscarTodosPais();
    }

    public List<Localizacao> buscarPeloPai(final Localizacao localizacao) {

        return this.localizacaoDAO.buscarPeloPai(localizacao);
    }

    public Localizacao buscarPeloCodigo(final String codigo) {

        return this.localizacaoDAO.buscarPeloCodigo(codigo);

    }
}
