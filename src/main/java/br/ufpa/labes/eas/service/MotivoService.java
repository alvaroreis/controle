package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.MotivoDAO;
import br.ufpa.labes.eas.model.Motivo;
import br.ufpa.labes.eas.model.TipoCongo;
import br.ufpa.labes.eas.util.Transacional;

public class MotivoService implements Serializable {

    private static final long serialVersionUID = -1189717982162047385L;

    @Inject
    private MotivoDAO motivoDAO;

    @Transacional
    public void salvar(final Motivo motivo) {

        this.motivoDAO.salvar(motivo);
    }

    @Transacional
    public void excluir(final Motivo motivo) {

        this.motivoDAO.excluir(motivo);
    }

    public List<Motivo> buscarTodos() {

        return this.motivoDAO.buscarTodos();

    }

    public List<Motivo> buscarPeloCongo(final TipoCongo tipoCongo) {

        return this.motivoDAO.buscarPeloCongo(tipoCongo);
    }

    public Motivo buscarPeloCodigo(final String codigo) {

        return this.motivoDAO.buscarPeloCodigo(codigo);

    }
}
