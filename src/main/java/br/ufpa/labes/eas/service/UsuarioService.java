package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.UsuarioDAO;
import br.ufpa.labes.eas.model.Usuario;
import br.ufpa.labes.eas.util.FacesMessages;
import br.ufpa.labes.eas.util.Transacional;

public class UsuarioService implements Serializable {

	private static final long serialVersionUID = -2815008177543027557L;

	@Inject
	private UsuarioDAO usuarioDAO;

	@Inject
	private FacesMessages messages;

	@Transacional
	public void salvar(final Usuario usuario) {
		this.usuarioDAO.salvar(usuario);
	}

	@Transacional
	public void excluir(final Usuario usuario) {

		this.usuarioDAO.excluir(usuario);
	}

	public List<Usuario> buscarTodos() {

		return this.usuarioDAO.buscarTodos();
	}

	public void bucarPeloCodigo(final String codigo) {

		this.usuarioDAO.buscarPeloCodigo(codigo);
	}

}
