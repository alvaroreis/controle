package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.CargoDAO;
import br.ufpa.labes.eas.model.Cargo;
import br.ufpa.labes.eas.util.Transacional;

public class CargoService implements Serializable {

    private static final long serialVersionUID = -6624272932001738326L;

    @Inject
    private CargoDAO cargoDAO;

    @Transacional
    public void salvar(final Cargo cargo) {

        this.cargoDAO.salvar(cargo);
    }

    @Transacional
    public void excluir(final Cargo cargo) {

        this.cargoDAO.excluir(cargo);
    }

    public List<Cargo> buscarTodos() {

        return this.cargoDAO.buscarTodos();
    }

    public void bucarPeloCodigo(final String codigo) {

        this.cargoDAO.buscarPeloCodigo(codigo);
    }

}
