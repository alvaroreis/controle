package br.ufpa.labes.eas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.ufpa.labes.eas.dao.CongoDAO;
import br.ufpa.labes.eas.model.Congo;
import br.ufpa.labes.eas.model.Usuario;
import br.ufpa.labes.eas.util.Transacional;

public class CongoService implements Serializable {

	private static final long serialVersionUID = -8580801057374848993L;

	@Inject
	private CongoDAO congoDAO;

	@Transacional
	public void salvar(final Congo congo) {

		this.congoDAO.salvar(congo);
	}

	@Transacional
	public void excluir(final Congo congo) {

		this.congoDAO.excluir(congo);
	}

	public List<Congo> buscarTodos() {

		return this.congoDAO.buscarTodos();
	}

	public List<Congo> congosPeloUsuario(final Usuario usuario) {
		return this.congoDAO.congosPeloUsuario(usuario);
	}

	public Congo buscarPeloCodigo(final String codigo) {

		return this.congoDAO.buscarPeloCodigo(codigo);
	}

	public Integer totalCongos(final Usuario usuario) {

		return this.congoDAO.totalCongos(usuario);
	}

}
