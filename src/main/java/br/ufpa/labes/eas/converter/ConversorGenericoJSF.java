package br.ufpa.labes.eas.converter;

import java.lang.reflect.Method;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "conversorGenericoJSF")
public class ConversorGenericoJSF implements Converter {

    @Override
    public Object getAsObject(final FacesContext context, final UIComponent component, final String value) {

        Object objeto = null;

        if ((value != null) && !value.isEmpty()) {

            objeto = component.getAttributes().get(value);
        }

        return objeto;
    }

    @Override
    public String getAsString(final FacesContext context, final UIComponent component, final Object value) {

        String chavePesquisa = "";

        if (value != null) {

            if (value instanceof Enum) {

                final Enum<?> enun = (Enum<?>) value;
                final String nome = enun.name();

                component.getAttributes().put(nome, enun);
                chavePesquisa = nome;

            } else {

                try {
                    final Method method = value.getClass().getMethod("getCodigo");
                    final String chavePrimaria = String.valueOf(method.invoke(value));

                    component.getAttributes().put(chavePrimaria, value);
                    chavePesquisa = chavePrimaria;
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return chavePesquisa;
    }
}