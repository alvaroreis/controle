package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.ufpa.labes.eas.model.Motivo;
import br.ufpa.labes.eas.model.TipoCongo;

public class MotivoDAO implements Serializable {

    private static final long serialVersionUID = -8266143309925845988L;

    @Inject
    private EntityManager em;

    public void salvar(final Motivo motivo) {

        this.em.merge(motivo);

    }

    public void excluir(Motivo motivo) {

        motivo = this.buscarPeloCodigo(motivo.getCodigo());

        this.em.remove(motivo);
        this.em.flush();
    }

    public List<Motivo> buscarTodos() {

        return this.em.createQuery("from Motivo", Motivo.class).getResultList();
    }

    public List<Motivo> buscarPeloCongo(final TipoCongo tipoCongo) {

        final TypedQuery<Motivo> query = this.em.createQuery("from Motivo m where m.tipoCongo = '" + tipoCongo.getCodigo() + "' ", Motivo.class);

        return query.getResultList();
    }

    public Motivo buscarPeloCodigo(final String codigo) {

        return this.em.find(Motivo.class, codigo);
    }

}
