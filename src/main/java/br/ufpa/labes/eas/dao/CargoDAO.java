package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.ufpa.labes.eas.model.Cargo;

public class CargoDAO implements Serializable {

    private static final long serialVersionUID = 2825425437849639085L;

    @Inject
    private EntityManager em;

    public void salvar(final Cargo cargo) {

        this.em.merge(cargo);
    }

    public void excluir(Cargo cargo) {

        cargo = this.buscarPeloCodigo(cargo.getCodigo());

        this.em.remove(cargo);
        this.em.flush();
    }

    public List<Cargo> buscarTodos() {

        return this.em.createQuery("from Cargo", Cargo.class).getResultList();
    }

    public Cargo buscarPeloCodigo(final String codigo) {

        return this.em.find(Cargo.class, codigo);
    }

}
