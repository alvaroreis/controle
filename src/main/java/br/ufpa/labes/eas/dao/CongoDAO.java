package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.ufpa.labes.eas.model.Congo;
import br.ufpa.labes.eas.model.Usuario;

public class CongoDAO implements Serializable {

	private static final long serialVersionUID = 9095931509905244844L;

	@Inject
	private EntityManager em;

	public void salvar(final Congo congo) {

		this.em.merge(congo);
	}

	public void excluir(Congo congo) {

		congo = this.buscarPeloCodigo(congo.getCodigo());

		this.em.remove(congo);
		this.em.flush();
	}

	public List<Congo> buscarTodos() {

		return this.em.createQuery("from Congo", Congo.class).getResultList();
	}

	public Congo buscarPeloCodigo(final String codido) {

		return this.em.find(Congo.class, codido);
	}

	public Integer totalCongos(final Usuario usuario) {

		final StringBuilder sb = new StringBuilder();

		sb.append("select m.NUM_PESO from Motivo m ");
		sb.append("join Congo c on m.ID_MOTIVO = c.ID_MOTIVO ");
		sb.append("join Usuario u on c.ID_USUARIO = u.ID_USUARIO ");
		sb.append("where u.ID_USUARIO = '" + usuario.getCodigo() + "' ");
		final List list = this.em.createNativeQuery(sb.toString()).getResultList();

		Integer peso = 0;
		Integer total = 0;
		for (final Object object : list) {

			peso = (Integer) object;
			total += peso;
		}
		return total;

	}

	@SuppressWarnings("unchecked")
	public List<Congo> congosPeloUsuario(final Usuario usuario) {
		final StringBuilder sb = new StringBuilder();
		sb.append("select * from Congo c ");
		sb.append("join Motivo m on c.ID_MOTIVO = m.ID_MOTIVO ");
		sb.append("join Usuario u on c.ID_USUARIO = u.ID_USUARIO ");
		sb.append("where u.ID_USUARIO = '" + usuario.getCodigo() + "' ");

		final List<Congo> congos = this.em.createNativeQuery(sb.toString(), Congo.class).getResultList();

		return congos;
	}
}
