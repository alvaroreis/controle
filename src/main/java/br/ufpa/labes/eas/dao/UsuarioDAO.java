package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.ufpa.labes.eas.model.Usuario;

public class UsuarioDAO implements Serializable {

    private static final long serialVersionUID = -6416056932924278521L;

    @Inject
    private EntityManager em;

    public void salvar(final Usuario usuario) {

        this.em.merge(usuario);
    }

    public void excluir(Usuario usuario) {

        usuario = this.buscarPeloCodigo(usuario.getCodigo());

        this.em.remove(usuario);
        this.em.flush();
    }

    public List<Usuario> buscarTodos() {

        return this.em.createQuery("from Usuario", Usuario.class).getResultList();
    }

    public Usuario buscarPeloCodigo(final String codigo) {

        return this.em.find(Usuario.class, codigo);

    }
}
