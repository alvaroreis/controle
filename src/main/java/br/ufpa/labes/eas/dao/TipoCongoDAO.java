package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.ufpa.labes.eas.model.TipoCongo;

public class TipoCongoDAO implements Serializable {

    private static final long serialVersionUID = -686699846978468354L;

    @Inject
    private EntityManager em;

    public void salvar(final TipoCongo tipoCongo) {

        this.em.merge(tipoCongo);
    }

    public void excluir(TipoCongo tipoCongo) {

        tipoCongo = this.buscarPeloCodigo(tipoCongo.getCodigo());
        this.em.remove(tipoCongo);
        this.em.flush();
    }

    public List<TipoCongo> buscarTodos() {

        return this.em.createQuery("from TipoCongo", TipoCongo.class).getResultList();
    }

    public TipoCongo buscarPeloCodigo(final String codigo) {

        return this.em.find(TipoCongo.class, codigo);
    }
}
