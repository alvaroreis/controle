package br.ufpa.labes.eas.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.ufpa.labes.eas.model.Localizacao;

public class LocalizacaoDAO implements Serializable {

    private static final long serialVersionUID = -3419299205804560341L;

    @Inject
    private EntityManager em;

    public void salvar(final Localizacao localizacao) {

        this.em.merge(localizacao);

    }

    public void excluir(Localizacao localizacao) {

        localizacao = this.buscarPeloCodigo(localizacao.getCodigo());

        this.em.remove(localizacao);
        this.em.flush();
    }

    public List<Localizacao> buscarTodosFilhos() {

        return this.em.createQuery("from Localizacao l where l.localizacaoPai is not null", Localizacao.class).getResultList();
    }

    public List<Localizacao> buscarTodosPais() {

        return this.em.createQuery("FROM Localizacao l where l.localizacaoPai is null", Localizacao.class).getResultList();
    }

    public List<Localizacao> buscarPeloPai(final Localizacao localizacao) {

        final TypedQuery<Localizacao> query = this.em.createQuery(
                        "FROM Localizacao l where l.localizacaoPai = '" + localizacao.getCodigo() + "' ", Localizacao.class);

        return query.getResultList();
    }

    public Localizacao buscarPeloCodigo(final String codigo) {

        return this.em.find(Localizacao.class, codigo);

    }
}
