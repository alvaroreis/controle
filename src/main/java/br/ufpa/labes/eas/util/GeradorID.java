package br.ufpa.labes.eas.util;

import java.util.UUID;

public final class GeradorID {

    private GeradorID() throws InstantiationException {

        throw new InstantiationException("Esta classe não pode ser instanciada.");
    }

    public static String toUUID() {

        return UUID.randomUUID().toString();
    }
}