package br.ufpa.labes.eas.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class CriptoSenha {

    private CriptoSenha() throws InstantiationException {

        throw new InstantiationException("Esta classe não pode ser instanciada.");
    }

    public static String toCripto(final String codigo) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        final MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
        final byte messageDigest[] = algorithm.digest(codigo.getBytes("UTF-8"));

        final StringBuilder hexString = new StringBuilder();
        for (final byte b : messageDigest) {
            hexString.append(String.format("%02X", 0xFF & b));
        }
        final String senha = hexString.toString();
        return senha;
    }
}
