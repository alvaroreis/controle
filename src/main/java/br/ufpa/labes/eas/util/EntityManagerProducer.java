package br.ufpa.labes.eas.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class EntityManagerProducer {

    private final EntityManagerFactory factory;

    public EntityManagerProducer() {

        this.factory = Persistence.createEntityManagerFactory("controlePU");
    }

    @Produces
    @RequestScoped
    public EntityManager createEntityManager() {

        return this.factory.createEntityManager();
    }

    public void closeEntityManager(@Disposes final EntityManager manager) {

        manager.close();
    }

}