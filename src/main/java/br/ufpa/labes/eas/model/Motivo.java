package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class Motivo implements Serializable {

    private static final long serialVersionUID = 3470437990440268111L;

    @Id
    @Column(name = "ID_MOTIVO", nullable = false)
    private String codigo;

    @NotEmpty
    @Column(name = "TX_MOTIVO", nullable = false)
    private String motivo;

    @OneToMany(mappedBy = "motivo")
    private List<Congo> congos;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CONGO", nullable = false)
    private TipoCongo tipoCongo;

    @Column(name = "NUM_PESO", nullable = false)
    private Integer peso;

    @PrePersist
    public void gerarChavePrimaria() {

        this.setCodigo(GeradorID.toUUID());
    }

    public String getCodigo() {

        return this.codigo;
    }

    public void setCodigo(final String codigo) {

        this.codigo = codigo;
    }

    public String getMotivo() {

        return this.motivo;
    }

    public void setMotivo(final String motivo) {

        this.motivo = motivo;
    }

    public List<Congo> getCongos() {

        return this.congos;
    }

    public void setCongos(final List<Congo> congos) {

        this.congos = congos;
    }

    public TipoCongo getTipoCongo() {

        return this.tipoCongo;
    }

    public void setTipoCongo(final TipoCongo tipoCongo) {

        this.tipoCongo = tipoCongo;
    }

    public Integer getPeso() {

        return this.peso;
    }

    public void setPeso(final Integer peso) {

        this.peso = this.tipoCongo.getPeso();
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Motivo other = (Motivo) obj;
        if (this.codigo == null) {
            if (other.codigo != null) {
                return false;
            }
        } else if (!this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

}
