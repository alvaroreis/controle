package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import org.hibernate.validator.constraints.NotEmpty;

import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class TipoCongo implements Serializable {

    private static final long serialVersionUID = 1834825203977328902L;

    @Id
    @Column(name = "ID_TIPO", nullable = false)
    private String codigo;

    @NotEmpty
    @Column(name = "TX_NOME", nullable = false)
    private String nome;

    @Column(name = "NUM_PESO", nullable = false)
    private Integer peso;

    @OneToMany(mappedBy = "tipoCongo")
    private List<Motivo> motivos;

    @PrePersist
    public void gerarChavePrimaria() {

        this.setCodigo(GeradorID.toUUID());
    }

    public String getCodigo() {

        return this.codigo;
    }

    public void setCodigo(final String codigo) {

        this.codigo = codigo;
    }

    public String getNome() {

        return this.nome;
    }

    public void setNome(final String nome) {

        this.nome = nome;
    }

    public Integer getPeso() {

        return this.peso;
    }

    public void setPeso(final Integer peso) {

        this.peso = peso;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final TipoCongo other = (TipoCongo) obj;
        if (this.codigo == null) {
            if (other.codigo != null) {
                return false;
            }
        } else if (!this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

}
