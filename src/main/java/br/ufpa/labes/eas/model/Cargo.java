package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import org.hibernate.validator.constraints.NotEmpty;

import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class Cargo implements Serializable {

    private static final long serialVersionUID = -8279417257969580748L;

    @Id
    @Column(name = "ID_CARGO", nullable = false)
    private String codigo;

    @NotEmpty
    @Column(name = "TX_NOME", nullable = false)
    private String nome;

    @OneToMany(mappedBy = "cargo", cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH })
    private List<Usuario> usuarios;

    @PrePersist
    public void gerarChavePrimaria() {

        this.setCodigo(GeradorID.toUUID());
    }

    public String getCodigo() {

        return this.codigo;
    }

    public void setCodigo(final String codigo) {

        this.codigo = codigo;
    }

    public String getNome() {

        return this.nome;
    }

    public void setNome(final String nome) {

        this.nome = nome.toUpperCase();
    }

    public List<Usuario> getUsuarios() {

        return this.usuarios;
    }

    public void setUsuarios(final List<Usuario> usuarios) {

        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Cargo other = (Cargo) obj;
        if (this.codigo == null) {
            if (other.codigo != null) {
                return false;
            }
        } else if (!this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

}
