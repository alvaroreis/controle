package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class Localizacao implements Serializable {

    private static final long serialVersionUID = 1930870729801180137L;

    @Id
    @Column(name = "ID_LOCALIZACAO")
    private String codigo;

    @NotEmpty
    @Column(name = "TX_NOME", nullable = false)
    private String nome;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_LOCALIZACAO_PAI")
    private Localizacao localizacaoPai;

    @OneToMany(mappedBy = "localizacaoPai", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<Localizacao> listaFilhos = null;

    @OneToMany(mappedBy = "localizacao", cascade = CascadeType.ALL)
    private List<Usuario> usuarios;

    @PrePersist
    public void gerarChavePrimaria() {

        this.setCodigo(GeradorID.toUUID());
    }

    public String getCodigo() {

        return this.codigo;
    }

    public void setCodigo(final String codigo) {

        this.codigo = codigo;
    }

    public String getNome() {

        return this.nome;
    }

    public void setNome(final String nome) {

        this.nome = nome.toUpperCase();
    }

    public Localizacao getLocalizacaoPai() {

        return this.localizacaoPai;
    }

    public void setLocalizacaoPai(final Localizacao localizacaoPai) {

        this.localizacaoPai = localizacaoPai;
    }

    public List<Localizacao> getListaFilhos() {

        return this.listaFilhos;
    }

    public void setListaFilhos(final List<Localizacao> listaFilhos) {

        this.listaFilhos = listaFilhos;
    }

    public List<Usuario> getUsuarios() {

        return this.usuarios;
    }

    public void setUsuarios(final List<Usuario> usuarios) {

        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Localizacao other = (Localizacao) obj;
        if (this.codigo == null) {
            if (other.codigo != null) {
                return false;
            }
        } else if (!this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

}
