package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class Congo implements Serializable {

	private static final long serialVersionUID = 6669016559543114560L;

	@Id
	@Column(name = "ID_CONGO", nullable = false)
	private String codigo;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "DT_CONGO")
	private Date data;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "ID_MOTIVO", nullable = false)
	private Motivo motivo;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "ID_USUARIO", nullable = false)
	private Usuario usuario;

	@PrePersist
	public void gerarChavePrimaria() {

		this.setCodigo(GeradorID.toUUID());
	}

	public String getCodigo() {

		return this.codigo;
	}

	public void setCodigo(final String codigo) {

		this.codigo = codigo;
	}

	public Motivo getMotivo() {

		return this.motivo;
	}

	public void setMotivo(final Motivo motivo) {

		this.motivo = motivo;
	}

	public Date getData() {

		return this.data;
	}

	public void setData(final Date data) {

		this.data = data;
	}

	public Usuario getUsuario() {

		return this.usuario;
	}

	public void setUsuario(final Usuario usuario) {

		this.usuario = usuario;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Congo other = (Congo) obj;
		if (this.codigo == null) {
			if (other.codigo != null) {
				return false;
			}
		} else if (!this.codigo.equals(other.codigo)) {
			return false;
		}
		return true;
	}

}
