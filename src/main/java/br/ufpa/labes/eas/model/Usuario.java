package br.ufpa.labes.eas.model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import br.ufpa.labes.eas.util.CriptoSenha;
import br.ufpa.labes.eas.util.GeradorID;

@Entity
public class Usuario implements Serializable {

    private static final long serialVersionUID = -3951342716086528376L;

    @Id
    @Column(name = "ID_USUARIO", nullable = false)
    private String codigo;

    @NotNull
    @Column(name = "TX_NOME", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "TX_USERNAME", nullable = false)
    private String username;

    @NotNull
    @Column(name = "TX_SENHA", nullable = false)
    private String senha;

    @Column(name = "BO_RADIO")
    private boolean radio;

    @Column(name = "BO_ADMINISTRADOR")
    private boolean administrador;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CARGO", nullable = false)
    private Cargo cargo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_LOCALIZACAO")
    private Localizacao localizacao;

    @Transient
    private Integer quantidadeTotal = 0;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Congo> congos;

    @PrePersist
    public void gerarChavePrimaria() {

        this.setCodigo(GeradorID.toUUID());
    }

    public String getCodigo() {

        return this.codigo;
    }

    public void setCodigo(final String codigo) {

        this.codigo = codigo;
    }

    public String getNome() {

        return this.nome;
    }

    public void setNome(final String nome) {

        this.nome = nome;
    }

    public String getUsername() {

        return this.username;
    }

    public void setUsername(final String username) {

        this.username = username.toLowerCase();
    }

    public String getSenha() {

        return this.senha;
    }

    public void setSenha(final String senha) {

        try {
            this.senha = CriptoSenha.toCripto(senha);
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public boolean isRadio() {

        return this.radio;
    }

    public void setRadio(final boolean radio) {

        this.radio = radio;
    }

    public boolean isAdministrador() {

        return this.administrador;
    }

    public void setAdministrador(final boolean administrador) {

        this.administrador = administrador;
    }

    public Cargo getCargo() {

        return this.cargo;
    }

    public void setCargo(final Cargo cargo) {

        this.cargo = cargo;
    }

    public Localizacao getLocalizacao() {

        return this.localizacao;
    }

    public void setLocalizacao(final Localizacao localizacao) {

        this.localizacao = localizacao;
    }

    public Integer getQuantidadeTotal() {

        return this.quantidadeTotal;
    }

    public void setQuantidadeTotal(final Integer quantidadeTotal) {

        this.quantidadeTotal = quantidadeTotal;
    }

    public List<Congo> getCongos() {

        return this.congos;
    }

    public void setCongos(final List<Congo> congos) {

        this.congos = congos;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.codigo == null) ? 0 : this.codigo.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.codigo == null) {
            if (other.codigo != null) {
                return false;
            }
        } else if (!this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

}
