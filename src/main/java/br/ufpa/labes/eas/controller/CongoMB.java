package br.ufpa.labes.eas.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.ufpa.labes.eas.model.Congo;
import br.ufpa.labes.eas.service.CongoService;
import br.ufpa.labes.eas.util.FacesMessages;

@Named
@ViewScoped
public class CongoMB implements Serializable {

    private static final long serialVersionUID = -8856430329581718259L;

    @Inject
    private FacesMessages messages;

    @Inject
    private CongoService congoService;

    private List<Congo> todosCongos;

    private Congo congoSelecionado;

    private Congo congoEdicao = new Congo();

    public void salvar() {

        try {
            this.congoService.salvar(this.congoEdicao);
            this.inicializar();

            this.messages.info("Congo salvo com sucesso!");

            PrimeFaces.current().ajax().update(Arrays.asList("frm-tabela-motivo:growl-global", "frm-tabela-motivo"));
        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }
    }

    public void excluir() {

        try {

            this.congoService.excluir(this.congoSelecionado);
            this.messages.info("Congo excluído com sucesso.");
            this.congoSelecionado = null;
            this.inicializar();

            PrimeFaces.current().ajax().update(Arrays.asList("frm-tabela-cargo:growl-global", "frm-tabela-cargo:tabela-cargo"));

        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }

    }

    public void inicializar() {

        this.limpar();
        this.todosCongos = this.congoService.buscarTodos();
    }

    public void limpar() {

        this.congoSelecionado = null;
        this.congoEdicao = new Congo();
    }

    public Congo getCongoSelecionado() {

        return this.congoSelecionado;
    }

    public void setCongoSelecionado(final Congo congoSelecionado) {

        this.congoSelecionado = congoSelecionado;
    }

    public Congo getCongoEdicao() {

        return this.congoEdicao;
    }

    public void setCongoEdicao(final Congo congoEdicao) {

        this.congoEdicao = congoEdicao;
    }

    public List<Congo> getTodosCongos() {

        return this.todosCongos;
    }

}
