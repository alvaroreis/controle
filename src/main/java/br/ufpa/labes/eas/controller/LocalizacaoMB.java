package br.ufpa.labes.eas.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.ufpa.labes.eas.model.Localizacao;
import br.ufpa.labes.eas.service.LocalizacaoService;
import br.ufpa.labes.eas.util.FacesMessages;

@Named
@ViewScoped
public class LocalizacaoMB implements Serializable {

    private static final long serialVersionUID = 3316045114305018869L;

    @Inject
    private LocalizacaoService localizacaoService;

    @Inject
    private FacesMessages messages;

    private Localizacao localizacaoEdicao = new Localizacao();

    private Localizacao localizacaoSelecionada;

    private List<Localizacao> todasLocalizacoesFilhas;

    private List<Localizacao> todasLocalizacoesPais;

    public void salvar() {

        try {

            this.localizacaoService.salvar(this.localizacaoEdicao);
            this.messages.info("Localização salva com sucesso.");

            this.inicializar();

            PrimeFaces.current().ajax()
                            .update(Arrays.asList("frm-tabela-localizacao:growl-localizacao", "frm-tabela-localizacao:tabela-localizacao"));
        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }

    }

    public void excluir() {

        try {

            this.localizacaoService.excluir(this.localizacaoSelecionada);
            this.messages.info("Usuário excluído com sucesso.");
            this.localizacaoSelecionada = null;
            this.inicializar();

            PrimeFaces.current().ajax()
                            .update(Arrays.asList("frm-tabela-localizacao:growl-localizacao", "frm-tabela-localizacao:tabela-localizacao"));

        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }

    }

    public void limpar() {

        // this.site = new Site();
        this.localizacaoSelecionada = null;

        this.localizacaoEdicao = new Localizacao();
    }

    public void inicializar() {

        this.limpar();
        this.todasLocalizacoesPais = this.localizacaoService.buscarTodosPai();
        this.todasLocalizacoesFilhas = this.localizacaoService.buscarTodosFilhos();
    }

    public List<Localizacao> getTodasLocalizacoesFilhas() {

        return this.todasLocalizacoesFilhas;
    }

    public List<Localizacao> getTodasLocalizacoesPais() {

        return this.todasLocalizacoesPais;
    }

    public Localizacao getLocalizacaoSelecionada() {

        return this.localizacaoSelecionada;
    }

    public void setLocalizacaoSelecionada(final Localizacao localizacaoSelecionada) {

        this.localizacaoSelecionada = localizacaoSelecionada;
    }

    public Localizacao getLocalizacaoEdicao() {

        return this.localizacaoEdicao;
    }

    public void setLocalizacaoEdicao(final Localizacao localizacaoEdicao) {

        this.localizacaoEdicao = localizacaoEdicao;
    }

}
