package br.ufpa.labes.eas.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.ufpa.labes.eas.dao.CongoDAO;
import br.ufpa.labes.eas.model.Cargo;
import br.ufpa.labes.eas.model.Congo;
import br.ufpa.labes.eas.model.Localizacao;
import br.ufpa.labes.eas.model.Motivo;
import br.ufpa.labes.eas.model.TipoCongo;
import br.ufpa.labes.eas.model.Usuario;
import br.ufpa.labes.eas.service.CongoService;
import br.ufpa.labes.eas.service.LocalizacaoService;
import br.ufpa.labes.eas.service.MotivoService;
import br.ufpa.labes.eas.service.UsuarioService;
import br.ufpa.labes.eas.util.FacesMessages;

@Named
@ViewScoped
public class UsuarioMB implements Serializable {

	private static final long serialVersionUID = -6703603993157230252L;

	@Inject
	private FacesMessages messages;

	@Inject
	private UsuarioService usuarioService;

	@Inject
	private MotivoService motivoService;

	@Inject
	private LocalizacaoService localizacaoService;

	@Inject
	private CongoService congoService;

	@Inject
	private CongoDAO congoDAO;

	private Usuario usuarioSelecionado;

	private Usuario usuarioEdicao = new Usuario();

	private Cargo cargoEdicao = new Cargo();

	private Congo congoEdicao = new Congo();

	private TipoCongo tipoCongoEdicao = new TipoCongo();

	private Localizacao localizacaoEdicao = new Localizacao();

	private List<Usuario> todosUsuarios;

	private List<Motivo> todosMotivosPeloCongo;

	private List<Congo> todosCongosPeloUsuario;

	private List<Localizacao> todasLocalizacoesPeloPai;

	public void salvar() {

		try {

			this.usuarioService.salvar(this.usuarioEdicao);
			this.messages.info("Usuário salvo com sucesso.");

			this.inicializar();

			PrimeFaces.current().ajax()
					.update(Arrays.asList("frm-tabela:growl-global", "frm-tabela:tabela-localizacao"));

		} catch (final Exception e) {
			this.messages.error(e.getMessage());
			e.printStackTrace();
		}

	}

	public void excluir() {

		try {

			this.usuarioService.excluir(this.usuarioSelecionado);
			this.messages.info("Usuário excluído com sucesso.");
			this.usuarioSelecionado = null;
			this.inicializar();

			PrimeFaces.current().ajax()
					.update(Arrays.asList("frm-tabela:growl-global", "frm-tabela:tabela-localizacao"));

		} catch (final Exception e) {
			this.messages.error(e.getMessage());
		}

	}

	public void adicionarInconformidade() {

		try {

			this.congoEdicao.setUsuario(this.usuarioEdicao);

			final Date data = new Date();
			this.congoEdicao.setData(data);

			this.congoService.salvar(this.congoEdicao);
			this.inicializar();

			this.messages.info("Inconformidade salva com sucesso!");
			PrimeFaces.current().ajax()
					.update(Arrays.asList("frm-tabela:growl-global", "frm-tabela:tabela-localizacao"));
		} catch (final Exception e) {
			this.messages.error(e.getMessage());
		}
	}

	public void alterarLocalizacao() {

		try {

			this.usuarioService.salvar(this.usuarioEdicao);
			this.messages.info("Localização alterada com sucesso.");

			this.inicializar();

			PrimeFaces.current().ajax()
					.update(Arrays.asList("frm-tabela:growl-global", "frm-tabela:tabela-localizacao"));
		} catch (final Exception e) {
			this.messages.error(e.getMessage());
		}

	}

	public void buscarPeloCongo() {

		this.todosMotivosPeloCongo = this.motivoService.buscarPeloCongo(this.tipoCongoEdicao);
	}

	public void buscarLocalizacaoPeloPai() {

		this.todasLocalizacoesPeloPai = this.localizacaoService.buscarPeloPai(this.localizacaoEdicao);
	}

	public void inicializar() {
		this.limpar();
		this.todosUsuarios = this.usuarioService.buscarTodos();
	}

	public void limpar() {

		this.usuarioSelecionado = null;

		this.tipoCongoEdicao = new TipoCongo();
		this.localizacaoEdicao = new Localizacao();
		this.congoEdicao = new Congo();
		this.usuarioEdicao = new Usuario();
	}

	public void preparalistaUsuariosPeloCongo() {
		if (this.usuarioSelecionado != null) {
			this.todosCongosPeloUsuario = this.congoDAO.congosPeloUsuario(this.usuarioSelecionado);
		}
	}

	public Usuario getUsuarioEdicao() {

		return this.usuarioEdicao;
	}

	public void setUsuarioEdicao(final Usuario usuarioEdicao) {

		this.usuarioEdicao = usuarioEdicao;
	}

	public Usuario getUsuarioSelecionado() {

		return this.usuarioSelecionado;
	}

	public void setUsuarioSelecionado(final Usuario usuarioSelecionado) {

		this.usuarioSelecionado = usuarioSelecionado;
	}

	public Cargo getCargoEdicao() {

		return this.cargoEdicao;
	}

	public void setCargoEdicao(final Cargo cargoEdicao) {

		this.cargoEdicao = cargoEdicao;
	}

	public Congo getCongoEdicao() {

		return this.congoEdicao;
	}

	public void setCongoEdicao(final Congo congoEdicao) {

		this.congoEdicao = congoEdicao;
	}

	public TipoCongo getTipoCongoEdicao() {

		return this.tipoCongoEdicao;
	}

	public void setTipoCongoEdicao(final TipoCongo tipoCongoEdicao) {

		this.tipoCongoEdicao = tipoCongoEdicao;
	}

	public Localizacao getLocalizacaoEdicao() {

		return this.localizacaoEdicao;
	}

	public void setLocalizacaoEdicao(final Localizacao localizacaoEdicao) {

		this.localizacaoEdicao = localizacaoEdicao;
	}

	public List<Usuario> getTodosUsuarios() {

		for (final Usuario usuario : this.todosUsuarios) {
			usuario.setQuantidadeTotal(this.congoService.totalCongos(usuario));
		}
		return this.todosUsuarios;
	}

	public List<Motivo> getTodosMotivosPeloCongo() {

		return this.todosMotivosPeloCongo;
	}

	public List<Congo> getTodosCongosPeloUsuario() {
		return this.todosCongosPeloUsuario;
	}

	public List<Localizacao> getTodasLocalizacoesPeloPai() {

		return this.todasLocalizacoesPeloPai;
	}

}
