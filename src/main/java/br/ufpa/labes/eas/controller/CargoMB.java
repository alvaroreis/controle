package br.ufpa.labes.eas.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.ufpa.labes.eas.model.Cargo;
import br.ufpa.labes.eas.service.CargoService;
import br.ufpa.labes.eas.util.FacesMessages;

@Named
@ViewScoped
public class CargoMB implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private CargoService cargoService;

    @Inject
    private FacesMessages messages;

    private List<Cargo> todosCargos;

    private Cargo cargoSelecionado;

    private Cargo cargoEdicao = new Cargo();

    public void salvar() {

        try {
            this.cargoService.salvar(this.cargoEdicao);
            this.inicializar();

            this.messages.info("Cargo salvo com sucesso!");

            PrimeFaces.current().ajax().update(Arrays.asList("frm-tabela-cargo:growl-global", "frm-tabela-cargo"));
        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }
    }

    public void excluir() {

        try {

            this.cargoService.excluir(this.cargoSelecionado);
            this.messages.info("Equipe excluído com sucesso.");
            this.cargoSelecionado = null;
            this.inicializar();

            PrimeFaces.current().ajax().update(Arrays.asList("frm-tabela-cargo:growl-global", "frm-tabela-cargo:tabela-cargo"));

        } catch (final Exception e) {
            this.messages.error("Não foi possivel excluir a equipe " + this.cargoSelecionado.getNome()
                            + ". Ela não deve possuir funcionários para poder ser excluída. ");
        }

    }

    public void inicializar() {

        this.limpar();
        this.todosCargos = this.cargoService.buscarTodos();
    }

    public void limpar() {

        this.cargoSelecionado = null;
        this.cargoEdicao = new Cargo();
    }

    public Cargo getCargoSelecionado() {

        return this.cargoSelecionado;
    }

    public void setCargoSelecionado(final Cargo cargoSelecionado) {

        this.cargoSelecionado = cargoSelecionado;
    }

    public Cargo getCargoEdicao() {

        return this.cargoEdicao;
    }

    public void setCargoEdicao(final Cargo cargoEdicao) {

        this.cargoEdicao = cargoEdicao;
    }

    public List<Cargo> getTodosCargos() {

        return this.todosCargos;
    }

}