package br.ufpa.labes.eas.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.ufpa.labes.eas.model.Motivo;
import br.ufpa.labes.eas.model.TipoCongo;
import br.ufpa.labes.eas.service.MotivoService;
import br.ufpa.labes.eas.service.TipoCongoService;
import br.ufpa.labes.eas.util.FacesMessages;

@Named
@ViewScoped
public class MotivoMB implements Serializable {

    private static final long serialVersionUID = -242578804412899747L;

    @Inject
    private MotivoService motivoService;

    @Inject
    private TipoCongoService tipoCongoService;

    @Inject
    private FacesMessages messages;

    private List<Motivo> todosMotivos;

    private Motivo motivoSelecionado;

    private Motivo motivoEdicao = new Motivo();

    private TipoCongo tipoCongoEdicao = new TipoCongo();

    public void salvar() {

        try {
            final Integer peso = this.motivoEdicao.getTipoCongo().getPeso();
            this.motivoEdicao.setPeso(peso);

            this.motivoService.salvar(this.motivoEdicao);
            this.inicializar();

            this.messages.info("Motivo salvo com sucesso!");

            PrimeFaces.current().ajax().update("frm-tabela-motivo:growl-global");
            PrimeFaces.current().ajax().update("frm-tabela-motivo");
        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }
    }

    public void excluir() {

        try {

            this.motivoService.excluir(this.motivoSelecionado);
            this.messages.info("Motivo excluído com sucesso.");
            this.motivoSelecionado = null;
            this.inicializar();

            PrimeFaces.current().ajax().update("frm-tabela-motivo:growl-global");
            PrimeFaces.current().ajax().update("frm-tabela-motivo");

        } catch (final Exception e) {
            this.messages.error(e.getMessage());
        }

    }

    public void inicializar() {

        this.limpar();
        this.todosMotivos = this.motivoService.buscarTodos();
    }

    public void limpar() {

        this.motivoSelecionado = null;

        this.tipoCongoEdicao = new TipoCongo();
        this.motivoEdicao = new Motivo();
    }

    public Motivo getMotivoSelecionado() {

        return this.motivoSelecionado;
    }

    public void setMotivoSelecionado(final Motivo motivoSelecionado) {

        this.motivoSelecionado = motivoSelecionado;
    }

    public Motivo getMotivoEdicao() {

        return this.motivoEdicao;
    }

    public void setMotivoEdicao(final Motivo motivoEdicao) {

        this.motivoEdicao = motivoEdicao;
    }

    public TipoCongo getTipoCongoEdicao() {

        return this.tipoCongoEdicao;
    }

    public void setTipoCongoEdicao(final TipoCongo tipoCongoEdicao) {

        this.tipoCongoEdicao = tipoCongoEdicao;
    }

    public List<Motivo> getTodosMotivos() {

        return this.todosMotivos;
    }

    public List<TipoCongo> getTiposCongos() {

        return this.tipoCongoService.buscarTodos();
    }

}