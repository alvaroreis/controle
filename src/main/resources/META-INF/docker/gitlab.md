# GitLab

[Docker Hub](https://hub.docker.com/r/gitlab/gitlab-ce/)
[GitLab](https://gitlab.com/)

## Volume de dados

```sh
sudo docker volume create --name gitlab_etc
sudo docker volume create --name gitlab_opt
sudo docker volume create --name gitlab_log
```

## Rede

```sh
Não precisa
```

## Primeira execução

```sh
docker run \
--name gitlab_8.15.3-ce.0 \
-p 9001:22 \
-p 9002:80 \
-p 9003:443 \
-v gitlab_etc:/etc/gitlab \
-v gitlab_opt:/var/opt/gitlab \
-v gitlab_log:/vat/log/gitlab \
--restart=unless-stopped \
-d gitlab/gitlab-ce:8.15.3-ce.0
```
