# Mysql

[Docker Hub](https://hub.docker.com/_/mysql/)
[Mysql](https://www.mysql.com/)

## Volume de dados

```sh
sudo docker volume create --name mysql_5.5_data
sudo docker volume create --name mysql_5.6_data

sudo docker volume create --name mysql_5.7_data
sudo docker volume create --name mysql_5.7_config

sudo docker volume create --name mysql_8.0.3_data
sudo docker volume create --name mysql_8.0.3_config
```

## Rede

```sh
Não precisa
```

## Primeira execução

### Mysql 5.5

```sh
docker run \
--name mysql_5.5 \
-p 2000:3306 \
-v mysql_5.5_data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=dev#2016 \
-e MYSQL_DATABASE=my-database \
-e MYSQL_USER=iec_desenv \
-e MYSQL_PASSWORD=iec_desenv \
-e MYSQL_ALLOW_EMPTY_PASSWORD=false \
--restart=unless-stopped \
-d \
mysql:5.5
```

### Mysql 5.6

```sh
docker run \
--name mysql_5.6 \
-p 2001:3306 \
-v mysql_5.6_data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=dev#2016 \
-e MYSQL_DATABASE=my-database \
-e MYSQL_USER=iec_desenv \
-e MYSQL_PASSWORD=iec_desenv \
-e MYSQL_ALLOW_EMPTY_PASSWORD=false \
--restart=unless-stopped \
-d \
mysql:5.6
```
### Mysql 5.7

```sh
docker run \
--name mysql_5.7 \
-p 2002:3306 \
-v mysql_5.7_config:/etc/mysql/conf.d \
-v mysql_5.7_data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=dev#2016 \
-e MYSQL_DATABASE=my-database \
-e MYSQL_USER=iec_desenv \
-e MYSQL_PASSWORD=iec_desenv \
-e MYSQL_ALLOW_EMPTY_PASSWORD=false \
--restart=unless-stopped \
-d \
mysql:5.7
```

### Mysql 8.0.3

```sh
docker run \
--name mysql_8.0.3 \
-p 3306:3306 \
-v mysql_8.0.3_config:/etc/mysql/conf.d \
-v mysql_8.0.3_data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=dev#2016 \
-e MYSQL_DATABASE=my-database \
-e MYSQL_USER=iec_desenv \
-e MYSQL_PASSWORD=iec_desenv \
-e MYSQL_ALLOW_EMPTY_PASSWORD=false \
--restart=unless-stopped \
-d \
mysql:8.0.3
```
