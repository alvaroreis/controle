--
-- Extraindo dados da tabela 'Cargo'
--

INSERT INTO 'Cargo' ('ID_CARGO', 'TX_NOME') VALUES
('3231a646-4c07-4b7d-8184-f4af8de17387', 'ATENDIMENTO'),
('5554cf5a-6c74-4271-a593-b0dd01e6e46e', 'PRODUÇÃO'),
('5c308d23-5878-4c83-bd24-4e50589518b1', 'DESENVOLVIMENTO'),
('9575e4f8-42fe-4d37-a075-b1312b03cdb3', 'MULTIPLATAFORMA'),
('cfc25f14-86e7-4149-b92c-ee63af984152', 'MONITORAMENTO'),
('de159dcb-6006-4dde-a05c-31a952570c08', 'CENTRAL DE SERVIÇOS');

-- --------------------------------------------------------
--
-- Extraindo dados da tabela 'TipoCongo'
--

INSERT INTO 'TipoCongo' ('ID_TIPO', 'TX_NOME', 'NUM_PESO') VALUES
('484747b6-6992-4b21-b69a-d5df870bbe4c', 'TECNICO', 5),
('ed7f729b-f11b-46d6-87df-486fec3ada39', 'DISCIPLINAR', 1);

-- --------------------------------------------------------
--
-- Extraindo dados da tabela 'Motivo'
--

INSERT INTO 'Motivo' ('ID_MOTIVO', 'TX_MOTIVO', 'NUM_PESO', 'ID_TIPO_CONGO') VALUES
('5d5d6747-a9d2-4f89-b40f-9c6437c20dd3', 'E-mail fora do padrão', 5, '484747b6-6992-4b21-b69a-d5df870bbe4c'),
('a7af8d40-fa0f-46b3-81b9-4b8d46fa4572', 'Uniforme fora do padrão', 1, 'ed7f729b-f11b-46d6-87df-486fec3ada39');

-- --------------------------------------------------------
--
-- Extraindo dados da tabela 'Congo'
--

INSERT INTO 'Congo' ('ID_CONGO', 'DT_CONGO', 'ID_MOTIVO', 'ID_USUARIO') VALUES
('78d30064-2555-43d3-a563-70e12298a5ee', '2018-10-10', '5d5d6747-a9d2-4f89-b40f-9c6437c20dd3', '5bf32e93-28bb-48d8-a576-60a44b9f6d04'),
('7e558431-ba23-4ede-ac24-4e99b7b5a045', '2018-10-10', 'a7af8d40-fa0f-46b3-81b9-4b8d46fa4572', '5bf32e93-28bb-48d8-a576-60a44b9f6d04'),
('8e780ccd-f5ec-4018-8c8c-92fc5f39319e', '2018-10-10', '5d5d6747-a9d2-4f89-b40f-9c6437c20dd3', '5bf32e93-28bb-48d8-a576-60a44b9f6d04');

-- --------------------------------------------------------
--
-- Extraindo dados da tabela 'Usuario'
--

INSERT INTO 'Usuario' ('ID_USUARIO', 'BO_ADMINISTRADOR', 'TX_NOME', 'BO_RADIO', 'TX_SENHA', 'TX_USERNAME', 'ID_CARGO', 'ID_LOCALIZACAO') VALUES
('5bf32e93-28bb-48d8-a576-60a44b9f6d04', b'1', 'Alvaro Luiz Reis Barros', b'0', 'A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3', 'alvarobarros', '5c308d23-5878-4c83-bd24-4e50589518b1', 'b512c0e5-2cc6-48f4-b83e-a94fff03eb00');
-- --------------------------------------------------------
--
-- Extraindo dados da tabela 'Localizacao'
--

INSERT INTO 'Localizacao' ('ID_LOCALIZACAO', 'TX_NOME', 'ID_LOCALIZACAO_PAI') VALUES
('50782a45-a177-4321-953c-014546319230', 'BEL', NULL),
('5daaa9d0-03bc-4c26-aa3b-8a117b49a366', 'OUTROS', NULL),
('b512c0e5-2cc6-48f4-b83e-a94fff03eb00', 'SOINF', 'ff3493d5-7605-4151-9f04-d2059828768c'),
('fcb8a87d-beea-4aef-b347-2d05b929aaea', 'CENP', NULL),
('ff3493d5-7605-4151-9f04-d2059828768c', 'ANA', NULL);
